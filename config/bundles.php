<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Symfony\Bundle\SecurityBundle\SecurityBundle::class => ['all' => true],
    Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class => ['dev' => true, 'test' => true],
    Infostrates\AwsPush\AwsPushBundle::class => ['dev' => true, 'test' => true],
];
