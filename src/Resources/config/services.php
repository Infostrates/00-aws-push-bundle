<?php

declare(strict_types=1);

use Infostrates\AwsPush\Bundle\DependencyInjection\AwsPushExtension;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use function Symfony\Component\DependencyInjection\Loader\Configurator\tagged_iterator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $parameters = $containerConfigurator->parameters();
    $services = $containerConfigurator->services();

    $services
        ->defaults()
        ->public()
        ->autowire()
        ->autoconfigure();

    $services->set(\Infostrates\AwsPush\AWS\AwsSnsConfig::class)
        ->autoconfigure(false)
        ->autowire(false);

    $services
        ->set(\Infostrates\AwsPush\AWS\AwsResolver::class)
        ->args([
            tagged_iterator(AwsPushExtension::AWS_CONFIG_TAG),
            '%' . AwsPushExtension::AWS_PARAMETER_DISABLE_PUSH . '%',
        ]);

    $services->load('Infostrates\AwsPush\\', __DIR__ . '/../../../src')
        ->exclude([
            __DIR__ . '/../../../src/Bundle',
            __DIR__ . '/../../../src/AwsPushBundle.php',
            __DIR__ . '/../../../src/Kernel.php',
            __DIR__ . '/../../../src/AWS/',
            __DIR__ . '/../../../src/Domain/Device.php',
            __DIR__ . '/../../../src/Resources',
        ]);
};
