<?php

/**
 * © Infostrates
 * Par julien
 * Le 25/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\Domain;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Infostrates\AwsPush\Contract\DeviceUser;

/**
 * @package Infostrates\Aws
 */
final class DeviceRepository extends ServiceEntityRepository implements DeviceGateway
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Device::class);
    }

    public function store(Device $device): bool
    {
        try {
            $this->getEntityManager()->persist($device);
            $this->getEntityManager()->flush();

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getSubscribed(string $deviceId): array
    {
        return $this->findBy(['deviceId' => $deviceId, 'subscribed' => true]);
    }

    public function findForDeviceUser(DeviceUser $entity): array
    {
        return $this->findForClassAndIndentifier($entity->getDeviceUserClassname(), $entity->getDeviceUserIdentifier());
    }

    public function findForClassAndIndentifier(string $deviceUserClass, string $deviceUserId): array
    {
        return $this->createQueryBuilder('o')
            ->where('o.deviceUserClass = :deviceUserClass')
            ->andWhere('o.deviceUserIdentifier = :deviceUserIdentifier')
            ->setParameter('deviceUserClass', $deviceUserClass)
            ->setParameter('deviceUserIdentifier', $deviceUserId)
            ->getQuery()
            ->getResult();
    }
}
