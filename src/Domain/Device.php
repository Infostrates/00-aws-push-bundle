<?php

/**
 * © Infostrates
 * Par julien
 * Le 25/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\Domain;

use Infostrates\AwsPush\Contract\DeviceInterface;
use Infostrates\AwsPush\Contract\DeviceUser;

/**
 * @package Infostrates\Aws
 */
class Device implements DeviceInterface
{
    private int $id;

    private string $deviceId;

    private string $deviceOs;

    private string $arn;

    private bool $subscribed = false;

    private string $locale;

    private \DateTimeImmutable $updatedAt;

    private string $deviceUserClass; // App\Domain\Tester\Entities\Tester

    private string $deviceUserIdentifier; // 42

    public function __construct(string $deviceId, string $deviceOs, string $locale, string $arn, DeviceUser $deviceUser = null)
    {
        $this->deviceId = $deviceId;
        $this->deviceOs = $deviceOs;
        $this->locale = $locale;
        $this->subscribed = true;
        $this->arn = $arn;
        if ($deviceUser !== null) {
            $this->deviceUserClass = $deviceUser->getDeviceUserClassname();
            $this->deviceUserIdentifier = $deviceUser->getDeviceUserIdentifier();
            $deviceUser->addDevice($this);
        }
        $this->subscribe($locale);
    }

    public function subscribe(string $locale): void
    {
        $this->subscribed = true;
        $this->locale = $locale;
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function unsubscribe(): void
    {
        $this->subscribed = false;
        $this->updatedAt = new \DateTimeImmutable();
    }

    public function isSubscribed(): bool
    {
        return $this->subscribed;
    }

    public function getDeviceId(): string
    {
        return $this->deviceId;
    }

    public function getDeviceOs(): string
    {
        return $this->deviceOs;
    }

    public function getArn(): string
    {
        return $this->arn;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function getDeviceUserClass(): string
    {
        return $this->deviceUserClass;
    }

    public function setDeviceUserClass(string $deviceUserClass): void
    {
        $this->deviceUserClass = $deviceUserClass;
    }

    public function getDeviceUserIdentifier(): string
    {
        return $this->deviceUserIdentifier;
    }

    public function setDeviceUserIdentifier(string $deviceUserIdentifier): void
    {
        $this->deviceUserIdentifier = $deviceUserIdentifier;
    }
}
