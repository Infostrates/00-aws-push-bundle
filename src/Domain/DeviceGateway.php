<?php

/**
 * © Infostrates
 * Par Julien
 * Le 23/03/2020
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\Domain;

use Infostrates\AwsPush\Contract\DeviceUser;

/**
 * @package Infostrates\Aws
 */
interface DeviceGateway
{
    public function store(Device $device): bool;

    public function getSubscribed(string $deviceId): array;

    public function findForDeviceUser(DeviceUser $entity): array;

    public function findForClassAndIndentifier(string $deviceUserClass, string $deviceUserId): array;
}
