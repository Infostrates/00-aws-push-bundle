<?php

/**
 * © Infostrates
 * Par julien
 * Le 25/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\Domain;

use Infostrates\AwsPush\AWS\AwsResolver;
use Infostrates\AwsPush\Contract\DeviceUser;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Security;

/**
 * @package Infostrates\Aws
 */
final class SubscriptionHandler
{
    private DeviceGateway $deviceGateway;

    private AwsResolver $awsResolver;

    private DeviceUser $deviceUser;

    private Security $security;

    public function __construct(
        DeviceGateway $deviceGateway,
        AwsResolver $awsResolver,
        Security $security
    ) {
        $this->deviceGateway = $deviceGateway;
        $this->awsResolver = $awsResolver;
        $this->security = $security;
    }

    public function handleSubscription(SubscriptionInput $input, string $method): bool
    {
        switch ($method) {
            case 'POST':
            default:
                $this->deviceUser = $this->retrieveDeviceUser();

                return $this->subscribe($input);
            case 'DELETE':
                return $this->unsubscribe($input);
        }
    }

    private function subscribe(SubscriptionInput $input): bool
    {
        $this->awsResolver->initialise($input->config);
        $device = $this->deviceUser->getDeviceById($input->deviceId);
        if (!$device instanceof Device) {
            $arn = $this->awsResolver->getDeviceArn($input->deviceOs, $input->deviceId);
            $device = new Device($input->deviceId, $input->deviceOs, $input->locale, $arn, $this->deviceUser);
        }
        $this->awsResolver->subscribe($device);
        $device->subscribe($input->locale);
        $this->deviceGateway->store($device);

        return $device->isSubscribed();
    }

    private function unsubscribe(SubscriptionInput $input): bool
    {
        $this->awsResolver->initialise($input->config);
        $devices = $this->deviceGateway->getSubscribed($input->deviceId);
        /** @var Device $device */
        foreach ($devices as $device) {
            $this->awsResolver->unsubscribe($device);
            $device->unsubscribe();
            $this->deviceGateway->store($device);
        }

        return false;
    }

    private function retrieveDeviceUser(): DeviceUser
    {
        $user = $this->security->getUser();
        if (!$user instanceof DeviceUser) {
            throw new UnsupportedUserException();
        }

        return $user;
    }
}
