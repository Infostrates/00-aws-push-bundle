<?php

/**
 * © Infostrates
 * Par Julien
 * Le 21/10/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\Domain;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Infostrates\AwsPush\Contract\DeviceInterface;

/**
 * @package Infostrates\Aws
 */
trait DeviceUserAwsMethods
{
    /** @var Collection<int, DeviceInterface> */
    private $devices;

    /**
     * @return Collection<int, DeviceInterface>
     */
    public function getDevices(): Collection
    {
        if (null === $this->devices) {
            $this->devices = new ArrayCollection();
        }

        return $this->devices;
    }

    /**
     * @return Collection<int, DeviceInterface>
     */
    public function getActiveDevices(): Collection
    {
        return $this->getDevices()->filter(function (DeviceInterface $device) {
            return $device->isSubscribed();
        });
    }

    public function getDeviceById(string $deviceId, bool $onlySubscribed = false): ?DeviceInterface
    {
        $toUse = $this->getDevices();
        if ($onlySubscribed) {
            $toUse = $this->getActiveDevices();
        }
        $filtered = $toUse->filter(function (DeviceInterface $device) use ($deviceId) {
            return $deviceId === $device->getDeviceId();
        })->first();
        if (!$filtered instanceof DeviceInterface) {
            return null;
        }

        return $filtered;
    }

    public function addDevice(DeviceInterface $device): void
    {
        $this->getDevices()->add($device);
    }

    public function getArns(): array
    {
        return $this->getActiveDevices()->map(fn(DeviceInterface $device) => $device->getArn())->toArray();
    }

    public function getDeviceUserClassname(): string
    {
        return static::class;
    }

    public function getDeviceUserIdentifier(): string
    {
        if (method_exists($this, 'getId')) {
            return (string) $this->getId();
        }
        if (property_exists($this, 'id')) {
            return (string) $this->id;
        }

        throw new \RuntimeException('You need to implement `getDeviceUserIdentifier` method');
    }
}
