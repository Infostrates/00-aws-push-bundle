<?php

/**
 * © Infostrates
 * Par julien
 * Le 25/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\Domain;

/**
 * @package Infostrates\Aws
 */
final class SubscriptionInput
{
    public string $deviceId;

    public string $deviceOs;

    public string $locale = 'en';

    public string $config = 'default';
}
