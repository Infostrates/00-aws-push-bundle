<?php

/**
 * © Infostrates
 * Par julien
 * Le 25/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\Bundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $builder = new TreeBuilder(AwsPushExtension::AWS_EXTENSION_NAME);
        $root = $builder->getRootNode();
        $root
            ->children()
                ->scalarNode('table_name')->defaultValue('aws_push__device')->end()
                ->booleanNode('disable_push')->defaultFalse()->end()
                ->scalarNode('key')->end()
                ->scalarNode('secret')->end()
                ->scalarNode('topic_arn')->end()
                ->scalarNode('android_application')->defaultNull()->end()
                ->scalarNode('ios_application')->defaultNull()->end()
            ->end()
            ->children()
                ->append($this->createConfigNode())
                ->end()
            ->end();

        return $builder;
    }

    private function createConfigNode(): ArrayNodeDefinition
    {
        $builder = new TreeBuilder('configs');
        $node = $builder->getRootNode();

        /** @var \Symfony\Component\Config\Definition\Builder\NodeBuilder $nodeChildren */
        $nodeChildren = $node->useAttributeAsKey('name')
            ->prototype('array')
            ->children();

        $nodeChildren
            ->scalarNode('key')->end()
            ->scalarNode('secret')->end()
            ->scalarNode('topic_arn')->end()
            ->scalarNode('android_application')->defaultNull()->end()
            ->scalarNode('ios_application')->defaultNull()->end();

        return $node;
    }
}
