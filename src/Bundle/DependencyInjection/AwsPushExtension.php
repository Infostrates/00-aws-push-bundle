<?php

/**
 * © Infostrates
 * Par julien
 * Le 25/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\Bundle\DependencyInjection;

use Infostrates\AwsPush\AWS\AwsSnsConfig;
use Infostrates\AwsPush\Bridge\EventSubscriber\AwsPushDoctrineSubscriber;
use Infostrates\AwsPush\Bridge\EventSubscriber\DeviceUserEntityListener;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;

final class AwsPushExtension extends Extension
{
    public const AWS_EXTENSION_NAME = 'ifs_aws_push';
    public const AWS_CONFIG_TAG = 'infostrates.aws.config';
    public const AWS_PARAMETER_DISABLE_PUSH = 'infostrates.aws.params.disable_push';
    public const AWS_PARAMETER_TABLE_NAME = 'infostrates.aws.params.table_name';
    private const DOCTRINE_EVENT_SUBSCRIBER_TAG = 'doctrine.event_subscriber';
    private const DOCTRINE_ENTITY_LISTENER_TAG = 'doctrine.orm.entity_listener';

    public function load(array $configs, ContainerBuilder $container): void
    {
        $phpFileLoader = new PhpFileLoader($container, new FileLocator(__DIR__ . '/../../Resources/config/'));
        $phpFileLoader->load('services.php');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $disablePush = filter_var($config['disable_push'], FILTER_VALIDATE_BOOL);

        $container->registerForAutoconfiguration(DeviceUserEntityListener::class)
            ->addTag(self::DOCTRINE_ENTITY_LISTENER_TAG);

        $container->registerForAutoconfiguration(AwsPushDoctrineSubscriber::class)
            ->addTag(self::DOCTRINE_EVENT_SUBSCRIBER_TAG);

        $container->setParameter(self::AWS_PARAMETER_DISABLE_PUSH, $disablePush);
        $container->setParameter(self::AWS_PARAMETER_TABLE_NAME, $config['table_name']);

        try {
            $this->validateConf($config);
        } catch (InvalidArgumentException $e) {
            if (!$disablePush) {
                throw $e;
            }
            $this->buildEmptyConf($container);

            return;
        }

        if (!empty($config['configs'])) {
            $this->defineMultipleConfigs($config, $container);
        } else {
            $this->defineNamedConfiguration('default', $config, $container);
        }
    }

    public function getAlias(): string
    {
        return self::AWS_EXTENSION_NAME;
    }

    private function validateConf(array $config): void
    {
        if (empty($config['configs']) && !isset($config['key'])) {
            throw new InvalidArgumentException('You have to define at least one configuration, using root keys or configs.');
        }
    }

    private function defineMultipleConfigs(array $config, ContainerBuilder $container): void
    {
        foreach ($config['configs'] as $name => $options) {
            $this->defineNamedConfiguration($name, $options, $container);
        }
    }

    private function defineNamedConfiguration(string $name, array $config, ContainerBuilder $container): void
    {
        $service = new Definition(AwsSnsConfig::class);
        $service->setArgument('$config', $name);
        $service->setArgument('$key', $config['key']);
        $service->setArgument('$secret', $config['secret']);
        $service->setArgument('$awsTopicArn', $config['topic_arn']);
        $service->setArgument('$awsAndroidApplication', $config['android_application']);
        $service->setArgument('$awsIosApplication', $config['ios_application']);
        $service->setPublic(true);

        $serviceName = sprintf('%s.config.%s', $this->getAlias(), $name);
        $container->setDefinition($serviceName, $service)->addTag(self::AWS_CONFIG_TAG);
        if ($name !== 'default') {
            $container->registerAliasForArgument($serviceName, AwsSnsConfig::class, $name . 'AwsConfig');
        }
    }

    private function buildEmptyConf(ContainerBuilder $container): void
    {
        $service = new Definition(AwsSnsConfig::class);
        $service->setArgument('$config', 'empty');
        $service->setArgument('$key', 'empty');
        $service->setArgument('$secret', 'empty');
        $service->setArgument('$awsTopicArn', 'empty');
        $service->setArgument('$awsAndroidApplication', 'empty');
        $service->setArgument('$awsIosApplication', 'empty');
        $service->setPublic(true);

        $serviceName = sprintf('%s.config.%s', $this->getAlias(), 'empty');
        $container->setDefinition($serviceName, $service)->addTag(self::AWS_CONFIG_TAG);
    }
}
