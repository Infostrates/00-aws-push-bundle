<?php

/**
 * © Infostrates
 * Par julien
 * Le 25/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\Bundle\DependencyInjection\Compiler;

use Infostrates\AwsPush\AWS\AwsResolver;
use Infostrates\AwsPush\Bundle\DependencyInjection\AwsPushExtension;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

final class AwsResolverCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        $snsConfigs = $container->findTaggedServiceIds(AwsPushExtension::AWS_CONFIG_TAG);
        if (count($snsConfigs) === 1) {
            $configId = array_keys($snsConfigs)[0];
            $configName = str_replace(AwsPushExtension::AWS_EXTENSION_NAME . '.config.', '', $configId);
            $resolver = $container->getDefinition(AwsResolver::class);
            $resolver->addMethodCall('initialise', [$configName]);
        }

        $disablePush = $container->getParameter(AwsPushExtension::AWS_PARAMETER_DISABLE_PUSH);
        if ($disablePush) {
            foreach ($snsConfigs as $id => $conf) {
                $config = $container->getDefinition($id);
                $config->addMethodCall('disable');
            }
        }
    }
}
