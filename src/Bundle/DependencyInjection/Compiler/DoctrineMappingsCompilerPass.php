<?php

/**
 * © Infostrates
 * Par julien
 * Le 25/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\Bundle\DependencyInjection\Compiler;

use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass;
use Infostrates\AwsPush\Bundle\DependencyInjection\AwsPushExtension;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\File\Exception\UnexpectedTypeException;

final class DoctrineMappingsCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        $xmlPath = (string) realpath(dirname(__DIR__, 4) . '/mapping/Device.orm.xml');
        $deviceMappingTemplate = (string) file_get_contents($xmlPath);
        $tableNameParam = $container->getParameter(AwsPushExtension::AWS_PARAMETER_TABLE_NAME);
        if (!is_string($tableNameParam)) {
            throw new UnexpectedTypeException($tableNameParam, 'Missing parameter ' . AwsPushExtension::AWS_PARAMETER_TABLE_NAME);
        }
        $tablename = $this->makeTableName($tableNameParam);
        $deviceMappingTemplate = str_replace($this->makeTableName('placeholder'), $tablename, $deviceMappingTemplate);
        file_put_contents($this->getOrmPath() . '/Device.orm.xml', $deviceMappingTemplate);
        $this->getORMCompilerPass()->process($container);
    }

    private function getORMCompilerPass(): DoctrineOrmMappingsPass
    {
        $mappings[$this->getOrmPath()] = 'Infostrates\\AwsPush\\Domain';

        return DoctrineOrmMappingsPass::createXmlMappingDriver($mappings);
    }

    private function makeTableName(string $value): string
    {
        return sprintf('table="%s"', $value);
    }

    private function getOrmPath(): string
    {
        return realpath(dirname(__DIR__, 3)) . '/Resources/config/orm';
    }
}
