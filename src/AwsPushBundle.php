<?php

/**
 * © Infostrates
 * Par julien
 * Le 25/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush;

use Infostrates\AwsPush\Bundle\DependencyInjection\AwsPushExtension;
use Infostrates\AwsPush\Bundle\DependencyInjection\Compiler\AwsResolverCompilerPass;
use Infostrates\AwsPush\Bundle\DependencyInjection\Compiler\DoctrineMappingsCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

final class AwsPushBundle extends Bundle
{
    public function getContainerExtension(): Extension
    {
        return new AwsPushExtension();
    }

    public function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new DoctrineMappingsCompilerPass());
        $container->addCompilerPass(new AwsResolverCompilerPass());
    }
}
