<?php

/**
 * © Infostrates
 * Par julien
 * Le 29/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\Contract;

interface DeviceInterface
{
    public function getDeviceId(): string;

    public function getDeviceOs(): string;

    public function getLocale(): string;

    public function getArn(): string;

    public function subscribe(string $locale): void;

    public function unsubscribe(): void;

    public function isSubscribed(): bool;

    public function getDeviceUserClass(): string;

    public function getDeviceUserIdentifier(): string;
}
