<?php

/**
 * © Infostrates
 * Par julien
 * Le 25/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\Contract;

interface DeviceUser
{
    public function addDevice(DeviceInterface $device): void;

    public function getDeviceById(string $deviceId, bool $onlySubscribed = false): ?DeviceInterface;

    public function getDeviceUserClassname(): string;

    public function getDeviceUserIdentifier(): string;
}
