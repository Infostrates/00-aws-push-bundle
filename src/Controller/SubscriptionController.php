<?php

/**
 * © Infostrates
 * Par julien
 * Le 24/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\Controller;

use Infostrates\AwsPush\Domain\SubscriptionHandler;
use Infostrates\AwsPush\Domain\SubscriptionInput;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class SubscriptionController extends AbstractController
{
    private SubscriptionHandler $subscriptionHandler;

    public function __construct(SubscriptionHandler $subscriptionHandler)
    {
        $this->subscriptionHandler = $subscriptionHandler;
    }

    public function subscribe(Request $req, SubscriptionInput $input): Response
    {
        $output = $this->subscriptionHandler->handleSubscription($input, $req->getMethod());

        return $this->json([], $output ? Response::HTTP_CREATED : Response::HTTP_NO_CONTENT);
    }
}
