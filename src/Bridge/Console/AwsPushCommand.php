<?php

/**
 * © Infostrates
 * Par julien
 * Le 24/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\Bridge\Console;

use Infostrates\AwsPush\AWS\AwsResolver;
use Infostrates\AwsPush\AWS\PushFactory;
use Infostrates\AwsPush\Contract\DeviceInterface;
use Infostrates\AwsPush\Contract\DeviceUser;
use Infostrates\AwsPush\Domain\DeviceGateway;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class AwsPushCommand extends Command
{
    private AwsResolver $awsResolver;

    private DeviceGateway $deviceGateway;

    private SymfonyStyle $io;

    public function __construct(AwsResolver $awsResolver, DeviceGateway $deviceGateway)
    {
        parent::__construct();
        $this->awsResolver = $awsResolver;
        $this->deviceGateway = $deviceGateway;
    }

    protected function configure(): void
    {
        $this->setName('ifs:aws:test-push');
        $this->addOption('config', null, InputOption::VALUE_OPTIONAL, 'Configuration used to push');
        $this->addOption('fqcn', null, InputOption::VALUE_OPTIONAL, 'FQCN of the Entity that implements DeviceUser');
        $this->addOption('id', null, InputOption::VALUE_OPTIONAL, 'Identifier of the instance (see `fqcn` option)');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io = new SymfonyStyle($input, $output);
        $config = $this->getConfig($input);
        $this->awsResolver->initialise($config);
        $additionalParams = $this->getParams();
        $arns = $this->getArns($input);

        $confirm = $this->io->confirm(sprintf('You are going to send a notification to %d devices. Continue ?', count($arns)), false);
        if (!$confirm) {
            $this->io->warning('Cancel action');

            return Command::SUCCESS;
        }
        $this->awsResolver->sendPush(PushFactory::create(PushFactory::SINGLE, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.', $arns, $additionalParams, 'Test notification'));

        return 0;
    }

    private function getHelperQuestion(): QuestionHelper
    {
        return $this->getHelper('question');
    }

    private function getConfig(InputInterface $input): string
    {
        $config = $input->getOption('config');
        if (null === $config) {
            $config = $this->io->choice('Select a config', $this->awsResolver->getAvailableConfigurations());
        }

        return $config;
    }

    private function getArns(InputInterface $input): array
    {
        $deviceUserClass = $this->getDeviceUserClass($input);
        $deviceUserId = $this->getDeviceUserIdentifier($input);
        $devices = $this->deviceGateway->findForClassAndIndentifier($deviceUserClass, $deviceUserId);
        if (empty($devices)) {
            throw new \RuntimeException(sprintf('No device found for user of class "%s" with identifier "%s"', $deviceUserClass, $deviceUserId));
        }

        $arns = array_map(fn(DeviceInterface $device) => $device->getArn(), $devices);
        if (empty($arns)) {
            throw new \RuntimeException('No arn registered for the given DeviceUser instance');
        }

        return $arns;
    }

    private function getDeviceUserClass(InputInterface $input): string
    {
        $deviceUserClass = $input->getOption('fqcn');
        if (null === $deviceUserClass) {
            $deviceUserClass = $this->io->ask('Type the FQCN of the Entity that implements DeviceUser interface');
        }
        $classImplements = class_implements($deviceUserClass);
        if (empty($classImplements) || !\in_array(DeviceUser::class, $classImplements)) {
            throw new \RuntimeException(sprintf('Class "%s" does not implement the DeviceUser interface', $deviceUserClass));
        }

        return $deviceUserClass;
    }

    private function getDeviceUserIdentifier(InputInterface $input): string
    {
        $deviceUserId = $input->getOption('id');
        if (null === $deviceUserId) {
            $deviceUserId = $this->io->ask('Type the ID of its instance');
        }

        return $deviceUserId;
    }

    private function getParams(): array
    {
        $out = [];
        $isFirstField = true;
        while (true) {
            if ($isFirstField) {
                $questionText = 'New param key (press <return> to stop adding param)';
            } else {
                $questionText = 'Add another parameter? Enter the key (or press <return> to stop adding param)';
            }
            $isFirstField = false;
            $key = $this->io->ask($questionText);
            if (null === $key) {
                break;
            }
            $valueQuestion = 'Now enter the value';
            $value = $this->io->ask($valueQuestion);
            if (null === $value) {
                $this->io->writeln('No value entered, try again');
                continue;
            }
            $out[$key] = $value;
        }

        return $out;
    }
}
