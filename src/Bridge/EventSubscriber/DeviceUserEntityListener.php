<?php

/**
 * © Infostrates
 * Par julien
 * Le 25/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\Bridge\EventSubscriber;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Infostrates\AwsPush\Contract\DeviceUser;
use Infostrates\AwsPush\Domain\DeviceGateway;

final class DeviceUserEntityListener
{
    private DeviceGateway $deviceGateway;

    public function __construct(DeviceGateway $deviceGateway)
    {
        $this->deviceGateway = $deviceGateway;
    }

    public function postLoad(DeviceUser $entity, LifecycleEventArgs $event): void
    {
        $devices = $this->deviceGateway->findForDeviceUser($entity);
        foreach ($devices as $device) {
            $entity->addDevice($device);
        }
    }
}
