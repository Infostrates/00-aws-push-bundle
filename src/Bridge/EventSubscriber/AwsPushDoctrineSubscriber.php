<?php

declare(strict_types=1);

namespace Infostrates\AwsPush\Bridge\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Infostrates\AwsPush\Contract\DeviceUser;

final class AwsPushDoctrineSubscriber implements EventSubscriber
{
    public function loadClassMetadata(LoadClassMetadataEventArgs $loadClassMetadataEventArgs): void
    {
        $classMetadata = $loadClassMetadataEventArgs->getClassMetadata();
        if ($classMetadata->reflClass === null) {
            return;
        }
        if ($classMetadata->isMappedSuperclass) {
            return;
        }

        if (is_a($classMetadata->reflClass->getName(), DeviceUser::class, true)) {
            $classMetadata->addEntityListener(Events::postLoad, DeviceUserEntityListener::class, 'postLoad');
        }
    }

    /**
     * @return string[]
     */
    public function getSubscribedEvents(): array
    {
        return [Events::loadClassMetadata];
    }
}
