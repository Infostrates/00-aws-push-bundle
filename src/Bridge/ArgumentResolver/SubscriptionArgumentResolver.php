<?php

/**
 * © Infostrates
 * Par julien
 * Le 25/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\Bridge\ArgumentResolver;

use Infostrates\AwsPush\Domain\SubscriptionInput;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

final class SubscriptionArgumentResolver implements ArgumentValueResolverInterface
{
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return $argument->getType() === SubscriptionInput::class && ($request->isMethod('post') || $request->isMethod('delete'));
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $value = json_decode((string) $request->getContent(false), true);
        if (empty($value)) {
            throw new \RuntimeException('No request data found');
        }
        $result = new SubscriptionInput();
        $result->deviceId = $value['deviceId'];
        $result->deviceOs = $value['deviceOs'];
        if (array_key_exists('config', $value)) {
            $result->config = $value['config'];
        }
        if (array_key_exists('locale', $value)) {
            $result->locale = $value['locale'];
        }

        yield $result;
    }
}
