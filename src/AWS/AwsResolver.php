<?php

/**
 * © Infostrates
 * Le 23/03/2020
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\AWS;

use Infostrates\AwsPush\AWS\Exception\InvalidOsException;
use Infostrates\AwsPush\AWS\ValueObjects\Message;
use Infostrates\AwsPush\AWS\ValueObjects\MultiplePush;
use Infostrates\AwsPush\AWS\ValueObjects\PushInterface;
use Infostrates\AwsPush\Contract\DeviceInterface;
use Infostrates\AwsPush\Domain\Device;

/**
 * @package Infostrates\Aws
 */
final class AwsResolver
{
    public static string $TMP_TOPIC = 'temp_topic_';

    private const LIST_OS_ALLOWED = ['ios', 'android'];

    private AwsSnsConfig $config;

    /** @var AwsSnsConfig[] */
    private array $configs = [];

    private bool $disablePush;

    private bool $init = false;

    public function __construct(
        iterable $configs,
        bool $disablePush
    ) {
        $this->disablePush = $disablePush;
        /** @var AwsSnsConfig $conf */
        foreach ($configs as $conf) {
            $this->configs[$conf->getConfig()] = $conf;
        }
    }

    public function initialise(string $type): AwsSnsConfig
    {
        if (!array_key_exists($type, $this->configs)) {
            throw new \RuntimeException(sprintf('No configuration named "%s". Available configurations are "%s"', $type, implode(', ', $this->getAvailableConfigurations())));
        }
        $this->config = $this->configs[$type];
        $this->init = true;

        return $this->config;
    }

    public function getAvailableConfigurations(): array
    {
        return array_keys($this->configs);
    }

    public function getCurrentConfig(): AwsSnsConfig
    {
        return $this->config;
    }

    public function sendPush(PushInterface $push): void
    {
        $this->validateInit();
        if ($this->disablePush) {
            return;
        }
        $topic = $this->config->getAwsTopicArn();
        switch (get_class($push)) {
            case PushFactory::MULTIPLE:
                $topic = $this->createTempTopic();
                $push->setTopicArn($topic);
                $this->subscribeMultiple($push);
                break;
            case PushFactory::BROADCAST:
                $push->setTopicArn($topic);
                break;
            case PushFactory::SINGLE:
            default:
                // Nothing to do
                break;
        }

        $this->sendNotification($push);
        if ($push instanceof MultiplePush) {
            $this->deleteTempTopic((string) $push->getTopicArn());
        }
    }

    public function subscribe(DeviceInterface $device): string
    {
        $this->validateInit();
        if ($this->disablePush) {
            return '';
        }
        $result = $this->config->subscribe($device->getArn());

        return (string) $result->get('SubscriptionArn');
    }

    public function unsubscribe(Device $device): void
    {
        $this->validateInit();
        if ($this->disablePush) {

            return;
        }
        $subscriptionArn = $this->subscribe($device);
        $subscriptionBroadcastArn = explode(':', $subscriptionArn);
        $id = (string) end($subscriptionBroadcastArn);

        $this->config->unsubscribe($device->getArn(), $subscriptionArn, $id);
    }

    public function getDeviceArn(string $deviceOs, string $deviceId): string
    {
        $this->validateInit();
        if ($this->disablePush) {

            return 'not subscribed';
        }
        if (!in_array($deviceOs, self::LIST_OS_ALLOWED)) {
            throw new InvalidOsException($deviceOs);
        }
        $result = $this->config->createPlatformEndpoint($deviceId, $this->getPlatformApplicationForDeviceType($deviceOs));

        return (string) $result->get('EndpointArn');
    }

    private function subscribeMultiple(PushInterface $push): void
    {
        $this->validateInit();
        if ($this->disablePush) {
            return;
        }
        foreach ($push->getArns() as $arnItem) {
            $endpoint = (is_array($arnItem) && array_key_exists('arn', $arnItem)) ? $arnItem['arn'] : $arnItem;
            $this->config->subscribe($endpoint, $push->getTopicArn());
        }
    }

    private function sendNotification(PushInterface $push): string
    {
        $result = $this->config->publish(new Message($push));

        return (string) $result->get('MessageId');
    }

    private function createTempTopic(): string
    {
        $topicName = sprintf(self::$TMP_TOPIC . '%s_%s', uniqid('', false), date('Y_m_d'));
        try {
            $result = $this->config->createTopic($topicName);

            return (string) $result->get('TopicArn');
        } catch (\Exception $e) {
            throw new \RuntimeException(sprintf('Unable to create topic : %s : %s', $topicName, $e->getMessage()));
        }
    }

    private function getPlatformApplicationForDeviceType(string $deviceOs): string
    {
        $method = sprintf('getAws%sApplication', $deviceOs);
        if (method_exists($this->config, $method)) {
            return $this->config->$method();
        }
        throw new InvalidOsException($deviceOs);
    }

    private function deleteTempTopic(string $topicArn): void
    {
        $this->config->deleteTopic($topicArn);
    }

    private function validateInit(): void
    {
        if (!$this->init) {
            throw new \RuntimeException('Not initialized. Use `initialisation` method before use');
        }
    }
}
