<?php

/**
 * © Infostrates
 * Le 23/03/2020
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\AWS\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @package Infostrates\Aws
 */
final class InvalidOsException extends HttpException
{
    public function __construct(string $os)
    {
        parent::__construct(405, 'Invalid OS ' . $os);
    }
}
