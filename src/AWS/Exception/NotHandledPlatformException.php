<?php

/**
 * © Infostrates
 * Par julien
 * Le 29/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\AWS\Exception;

final class NotHandledPlatformException extends \InvalidArgumentException
{
    /** @var int */
    protected $code = 405;

    public function __construct(string $platform)
    {
        parent::__construct(sprintf('Platform `%s` not handled by the application', $platform));
    }
}
