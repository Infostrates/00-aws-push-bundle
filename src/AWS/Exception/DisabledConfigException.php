<?php

/**
 * © Infostrates
 * Par julien
 * Le 29/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\AWS\Exception;

final class DisabledConfigException extends \Exception
{
    public function __construct(string $config)
    {
        parent::__construct(sprintf('The AwsSnsConfig `%s` is disabled', $config));
    }
}
