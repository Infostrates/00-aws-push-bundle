<?php

/**
 * © Infostrates
 * Le 23/03/2020
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\AWS;

use Infostrates\AwsPush\AWS\ValueObjects\BroadcastPush;
use Infostrates\AwsPush\AWS\ValueObjects\MultiplePush;
use Infostrates\AwsPush\AWS\ValueObjects\PushInterface;
use Infostrates\AwsPush\AWS\ValueObjects\SinglePush;

/**
 * @package Infostrates\Aws
 */
class PushFactory
{
    public const BROADCAST = BroadcastPush::class;
    public const MULTIPLE = MultiplePush::class;
    public const SINGLE = SinglePush::class;

    public static function create(string $className, string $message, array $arns = [], array $additionalParams = [], ?string $title = null): PushInterface
    {
        switch ($className) {
            case self::BROADCAST:
                return new BroadcastPush($message, $additionalParams, $arns, $title);
            case self::MULTIPLE:
                return new MultiplePush($message, $additionalParams, $arns, $title);
            case self::SINGLE:
                return new SinglePush($arns, $message, $additionalParams, $title);
            default:
                throw new \RuntimeException(sprintf('Unknown Push class %s', $className));
        }
    }
}
