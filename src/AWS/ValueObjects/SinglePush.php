<?php

/**
 * © Infostrates
 * Le 23/03/2020
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\AWS\ValueObjects;

/**
 * @package Infostrates\Aws
 */
class SinglePush implements PushInterface
{
    protected string $topicArn;

    protected string $message;

    protected array $extraParams = [];

    private ?string $title = null;

    public function __construct(array $arns, string $message, array $extraParams, ?string $title = null)
    {
        $this->topicArn = (string) current($arns);
        $this->message = $message;
        $this->extraParams = $extraParams;
        $this->title = $title;
    }

    public function getTopicArn(): string
    {
        return $this->topicArn;
    }

    public function setTopicArn(string $topicArn): PushInterface
    {
        $this->topicArn = $topicArn;

        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getExtraParams(): array
    {
        return $this->extraParams;
    }

    public function getArns(): array
    {
        return [$this->topicArn];
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }
}
