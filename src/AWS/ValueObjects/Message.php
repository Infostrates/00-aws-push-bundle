<?php

/**
 * © Infostrates
 * Par Julien
 * Le 02/09/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\AWS\ValueObjects;

/**
 * @package Infostrates\Aws
 */
final class Message
{
    private PushInterface $push;

    public function __construct(PushInterface $push)
    {
        $this->push = $push;
    }

    public function getTopicArn(): ?string
    {
        return $this->push->getTopicArn();
    }

    public function json(): string
    {
        $messageArray = [
            'default' => $this->push->getMessage(),
            'APNS' => $this->getApnsJsonInner(), // ApplePushNotificationServer
            'APNS_SANDBOX' => $this->getApnsJsonInner(), // ApplePushNotificationServer
            'FCM' => $this->getFcmJsonInner(), // Firebase Cloud Messaging
            'GCM' => $this->getFcmJsonInner(), // Google Cloud Messaging
            'ADM' => $this->getAdmJsonInner(), // Amazon Device Messaging
        ];

        return $this->encode($messageArray);
    }

    private function getAdmJsonInner(): string
    {
        $adm = [
            'data' => array_merge([
                'message' => $this->push->getMessage(),
                'title' => $this->push->getTitle(),
            ], $this->push->getExtraParams()),
            'expiresAfter' => null,
        ];

        return $this->encode($adm);
    }

    private function getApnsJsonInner(): string
    {
        $apns = array_merge([
            'aps' => [
                'alert' => [
                    'body' => $this->push->getMessage(),
                    'title' => $this->push->getTitle(),
                ],
            ],
        ], $this->push->getExtraParams());

        return $this->encode($apns);
    }

    private function getFcmJsonInner(): string
    {
        $fcm = [
            'collapse_key' => 'do_not_collapse',
            'time_to_live' => null,
            'delay_while_idle' => false,
            'priority' => 'high',
            'notification' => [
                'body' => $this->push->getMessage(),
                'title' => $this->push->getTitle(),
                'priority' => 'high',
            ],
            'data' => array_merge([
                'message' => $this->push->getMessage(),
                'title' => $this->push->getTitle(),
            ], $this->push->getExtraParams()),
        ];

        return $this->encode($fcm);
    }

    private function encode(array $data): string
    {
        $json = json_encode($data, JSON_UNESCAPED_UNICODE);
        if ($json === false) {
            throw new \RuntimeException('Could not json_encode data');
        }

        return $json;
    }
}
