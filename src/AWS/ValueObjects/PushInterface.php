<?php

/**
 * © Infostrates
 * Le 23/03/2020
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\AWS\ValueObjects;

/**
 * @package Infostrates\Aws
 */
interface PushInterface
{
    public function getTopicArn(): ?string;

    public function setTopicArn(string $topicArn): PushInterface;

    public function getMessage(): string;

    public function getExtraParams(): array;

    public function getArns(): array;

    public function getTitle(): ?string;
}
