<?php

/**
 * © Infostrates
 * Le 23/03/2020
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\AWS\ValueObjects;

/**
 * @package Infostrates\Aws
 */
class MultiplePush implements PushInterface
{
    private array $arns = [];

    private ?string $topicArn = null;

    private string $message;

    private array $extraParams = [];

    private ?string $title = null;

    public function __construct(string $message, array $extraParams, array $arns = [], ?string $title = null)
    {
        $this->message = $message;
        $this->extraParams = $extraParams;
        $this->arns = $arns;
        $this->title = $title;
    }

    public function getArns(): array
    {
        return $this->arns;
    }

    public function getTopicArn(): ?string
    {
        return $this->topicArn;
    }

    public function setTopicArn(string $topicArn): PushInterface
    {
        $this->topicArn = $topicArn;

        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getExtraParams(): array
    {
        return $this->extraParams;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }
}
