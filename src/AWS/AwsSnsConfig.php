<?php

/**
 * © Infostrates
 * Par Julien
 * Le 26/08/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\AWS;

use Aws\Sns\SnsClient;
use Infostrates\AwsPush\AWS\Exception\DisabledConfigException;
use Infostrates\AwsPush\AWS\Exception\NotHandledPlatformException;
use Infostrates\AwsPush\AWS\ValueObjects\Message;

/**
 * @package Infostrates\Aws
 */
final class AwsSnsConfig
{
    private string $config;

    private string $awsTopicArn;

    private ?string $awsAndroidApplication;

    private ?string $awsIosApplication;

    private SnsClient $client;

    private bool $disabled = false;

    public function __construct(
        string $config,
        string $key,
        string $secret,
        string $awsTopicArn,
        ?string $awsAndroidApplication = null,
        ?string $awsIosApplication = null
    ) {
        $this->awsTopicArn = $awsTopicArn;
        $this->awsAndroidApplication = $awsAndroidApplication;
        $this->awsIosApplication = $awsIosApplication;
        $this->client = new SnsClient([
            'region' => 'eu-west-1',
            'version' => '2010-03-31',
            'credentials' => [
                'key' => $key,
                'secret' => $secret,
            ],
            'scheme' => 'http',
            'http_handler' => \Aws\default_http_handler(),
        ]);

        $this->config = $config;
    }

    public function disable(): void
    {
        $this->disabled = true;
    }

    public function getAwsTopicArn(): string
    {
        return $this->awsTopicArn;
    }

    public function getAwsAndroidApplication(): string
    {
        if (null === $this->awsAndroidApplication) {
            throw new NotHandledPlatformException('Android');
        }

        return $this->awsAndroidApplication;
    }

    public function getAwsIosApplication(): string
    {
        if (null === $this->awsIosApplication) {
            throw new NotHandledPlatformException('Ios');
        }

        return $this->awsIosApplication;
    }

    public function getConfig(): string
    {
        return $this->config;
    }

    public function subscribe(string $endpoint, ?string $arn = null): \Aws\Result
    {
        if (null === $arn) {
            $arn = $this->getAwsTopicArn();
        }

        return $this->getClient()->subscribe(
            [
                'Endpoint' => $endpoint,
                'Protocol' => 'application',
                'TopicArn' => $arn,
            ]
        );
    }

    public function unsubscribe(string $endpoint, string $subscriptionArn, string $subscriptionId): \Aws\Result
    {
        return $this->getClient()->unsubscribe(
            [
                'EndpointArn' => $endpoint,
                'SubscriptionArn' => $subscriptionArn,
                'SubscriptionId' => $subscriptionId,
            ]);
    }

    public function createPlatformEndpoint(string $deviceId, string $platform): \Aws\Result
    {
        return $this->getClient()->createPlatformEndpoint(
            [
                'PlatformApplicationArn' => $platform,
                'Token' => $deviceId,
            ]
        );
    }

    public function publish(Message $message): \Aws\Result
    {
        return $this->getClient()->publish(
            [
                'Message' => $message->json(),
                'TargetArn' => $message->getTopicArn(),
                'MessageStructure' => 'json',
                'MessageAttributes' => [
                    'AWS.SNS.MOBILE.APNS.PUSH_TYPE' => ['DataType' => 'String', 'StringValue' => 'alert'],
                ],
            ]
        );
    }

    public function createTopic(string $topicName): \Aws\Result
    {
        return $this->getClient()->createTopic(['Name' => $topicName]);
    }

    public function deleteTopic(string $topicArn): \Aws\Result
    {
        return $this->getClient()->deleteTopic(['TopicArn' => $topicArn]);
    }

    private function getClient(): SnsClient
    {
        if ($this->disabled) {
            throw new DisabledConfigException($this->config);
        }

        return $this->client;
    }
}
