##
## PROJECT
## -------
.DEFAULT_GOAL := help

help: ## Default goal (display the help message)
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-20s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

.PHONY: help

##
## ALIASES
## -------
install: ## Install php & node depencies
install: composer

test: ## Run every tests
test: stan unit metrics

##
## MANAGE DEPENDENCIES
## ------
composer: ## Install composer dependecies
composer:
	composer install -n --prefer-dist

fixture: ## Load fixtures
fixture:
	php bin/console app:test:fixtures

dump-sql: ## Dump sql schema
dump-sql:
	php bin/console d:s:u --dump-sql

dsuf: ## d:s:u --force
dsuf:
	php bin/console d:s:u --force

##
## TESTS
## ------
unit: ## Run phpunit unit tests
	APP_ENV=test make dsuf
	php vendor/bin/simple-phpunit

stan: ## Run phpstan code analyze
	vendor/bin/phpstan analyze src

metrics: ## Run phpmetrics and moves the directory to be accessible via localhost/phpmetrics/index.html
	php vendor/bin/phpmetrics --config=.phpmetrics.json
	php phpmetrics_utils.php analyse

metrics-analyse: ## Run analysis on phpmetrics
	php phpmetrics_utils.php analyse

metrics-generate: ## Saves the latest results into baseline file for analysis
	php phpmetrics_utils.php generate
