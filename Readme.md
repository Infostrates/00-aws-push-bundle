# Infostrates AWS Push Bundle

## How to use ?

1. Add the following to your composer.json, then `composer req infostrates/aws-push-bundle`

# TODO : voir https://getcomposer.org/doc/03-cli.md#modifying-repositories 

```json
{
    "repositories": [
        {
            "name": "infostrates/aws-push-bundle",
            "type": "git",
            "url": "git@bitbucket.org:Infostrates/00-aws-push-bundle.git"
        }
    ]
}
```

2. Make your User class implement the `Infostrates\AwsPush\Contract\DeviceUser` interface, and use the `Infostrates\AwsPush\Domain\DeviceUserAwsMethods` trait 

```php
<?php 
...
use Infostrates\AwsPush\Contract\DeviceUser;
use Infostrates\AwsPush\Domain\DeviceUserAwsMethods;
use Symfony\Component\Security\Core\User\UserInterface;

class Tester implements UserInterface, DeviceUser
{
    use DeviceUserAwsMethods;
    ...
}
```
3. Complete the bundle configuration to fit your AWS account  
4. Subscribe your DeviceUser using the given route  
5. Use the `Infostrates\AwsPush\AWS\AwsResolver` service to send a message to AWS ([example](docs/push_example.md))   

## Configuration samples

### Disabling push on dev & test environments 

```yaml
# config/packages/dev/ifs_aws_push.yaml
# config/packages/test/ifs_aws_push.yaml

ifs_aws_push:
  disable_push: false  
```

### Service configuration
```yaml
# config/packages/ifs_aws_push.yaml

ifs_aws_push:
    key:                  '%env(AWS_PUSH_KEY)%'
    secret:               '%env(AWS_PUSH_SECRET)%'
    topic_arn:            '%env(AWS_TOPIC_ARN)%'
    android_application:  '%env(AWS_PUSH_ANDROID_ARN)%'
    ios_application:      '%env(AWS_PUSH_IOS_ARN)%'
```
A service with id `ifs_aws_push.config.default` will be available, as well as an `Infostrates\AwsPush\AWS\AwsSnsConfig $config` configured.

The table `aws_push__device` will be used to map with doctrine entity.

Use `bin/console make:migration` && `bin/console doctrine:migrations:migrate` 
to create the table, or create by hand. 

### Adding the route for subscription/unsubcription

Feel free to change the `prefix` to suit your needs.

* The SUBSCRIPTION route needs to be behind a Symfony Security firewall
* The UNSUBSCRIPTION route can be call without firewall
     - `{ path: ^/api/aws/subscription, roles: PUBLIC_ACCESS, methods: [ "delete" ] }`

Check `lexik/jwt-authentication-bundle` for reference

```yaml
# config/routes/ifs_aws_push.yaml

ifs_aws_push:
  resource: '@AwsPushBundle/Resources/config/routing/awspush.xml'
  prefix: /api
```

### More complex configuration example
```yaml
# config/packages/ifs_aws_push.yaml

ifs_aws_push:
  table_name:               push__device
  configs:
    pos:
      key:                  '%env(AWS_PUSH_KEY)%'
      secret:               '%env(AWS_PUSH_SECRET)%'
      topic_arn:            '%env(AWS_TOPIC_ARN)%'
      android_application:  '%env(AWS_PUSH_ANDROID_ARN)%'
      ios_application:      '%env(AWS_PUSH_IOS_ARN)%'

    vet:
      key:                  '%env(AWS_VET_PUSH_KEY)%'
      secret:               '%env(AWS_VET_PUSH_SECRET)%'
      topic_arn:            '%env(AWS_VET_TOPIC_ARN)%'
      android_application:  '%env(AWS_VET_PUSH_ANDROID_ARN)%'
      ios_application:      '%env(AWS_VET_PUSH_IOS_ARN)%'
```

## Config reference

```yaml
# config/packages/ifs_aws_push.yaml

ifs_aws_push:
    table_name: aws_push__device  # changes the table name used
    disable_push: false           # disables the push as well as all the AwsSnsConfig services
                                  # a Infostrates\AwsPush\AWS\Exception\DisabledConfigException will be thrown if used with a config
    key: ~
    secret: ~
    topic_arn: ~
    android_application: null
    ios_application: null
    
    # Prototype                   # if none defined, the above configuration will be default
    configs:                      # if used, the default one will be ignored
      name:                       # service with ifs_aws_push.config.name will be created
                                  # as well as Infostrates\AwsPush\AWS\AwsSnsConfig $nameAwsConfig
        key: ~
        secret: ~
        topic_arn: ~
        android_application: null # if null ios_application is mandatory
        ios_application: null     # if null android_application is mandatory
```

## Use of the sub/unsub

```yaml
# SUBSCRIBE
# POST {base_uri}/api/aws/subscription
{
    "deviceId": "ctbNh0qrTCS77TW-bPPsjV:APA91bFkZs6x33nuodI808w4QJqNl0FAZ8Q9GZfK6FO--gKbmi0Ofz1jJiifKXy9lLS0t2zMbwVoRELO73i3avzyq60O64BAL-j_U4RB1vf7Dh2DPIH9cLg5b8mmyyDbPRj6ynXu7leH",
    "deviceOs": "android"
}
# returns HTTP 201
```

```yaml
# UNSUBSCRIBE
# DELETE {base_uri}/api/aws/subscription
{
    "deviceId": "ctbNh0qrTCS77TW-bPPsjV:APA91bFkZs6x33nuodI808w4QJqNl0FAZ8Q9GZfK6FO--gKbmi0Ofz1jJiifKXy9lLS0t2zMbwVoRELO73i3avzyq60O64BAL-j_U4RB1vf7Dh2DPIH9cLg5b8mmyyDbPRj6ynXu7leH",
    "deviceOs": "android"
}
# returns HTTP 204
```

The default configuration will be used.   
If needed, you can pass the configuration as well as the device locale (for translations purpose) in the subscription

```yaml
{
    "deviceId": "ctbNh0qrTCS77TW-bPPsjV:APA91bFkZs6x33nuodI808w4QJqNl0FAZ8Q9GZfK6FO--gKbmi0Ofz1jJiifKXy9lLS0t2zMbwVoRELO73i3avzyq60O64BAL-j_U4RB1vf7Dh2DPIH9cLg5b8mmyyDbPRj6ynXu7leH",
    "deviceOs": "android",
    "locale": "fr",
    "config": "pos"
}
```
