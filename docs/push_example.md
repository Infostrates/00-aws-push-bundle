# Full implementation example

```php
class PushHandler
{
    // attributes & services
    // ...
    
    public function execute(Protocol $protocol): void
    {
        $settings = $this->pushSettingGateway->findAllActive($protocol);
        $tester = $this->testerGateway->authenticate();
        $this->awsResolver->initialise($protocol->getType());
        $this->populatePushs($tester, $settings);
    }
    
    private function populatePushs(Tester $tester, array $settings): void
    {
        $messages = new Messages();
        /**
        * Do something, populate the messages
        */ 
        $this->sendMessages($messages, $tester->getDevices());
    }
    
    private function sendMessages(Messages $messages, Collection $devices): void 
    {
        $arns = [];
        /** @var Device $device */
        foreach ($devices as $device) {
            $arns[$device->getLocale()][] = $device->getArn();
        }
        if (empty($arns)) {
            return;
        }
        /** @var PushMessageModel $message */
        foreach ($messages as $message) {
            foreach ($arns as $locale => $arnByLocale) {
                $trad = $this->transPush($message, $locale);
                $this->sendPush($trad, $arnByLocale, $message->options);
            }
        }
    }
    
    private function sendPush(string $message, array $arns, array $options, ?string $title = null): void
    {
        if (empty($arns)) {
            return;
        }
        $this->awsResolver->sendPush(
            PushFactory::create(PushFactory::MULTIPLE, $message, $arns, $options, $title)
        );
    }
}
```
