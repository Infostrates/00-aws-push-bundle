<?php

/**
 * © Infostrates
 * Par julien
 * Le 29/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\Tests\Services;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Infostrates\AwsPush\Contract\DeviceUser;
use Infostrates\AwsPush\Domain\Device;
use Infostrates\AwsPush\Domain\DeviceGateway;
use Infostrates\AwsPush\Tests\Fixtures\Entity\DeviceEntity;

final class DeviceEntityRepository extends ServiceEntityRepository implements DeviceGateway
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DeviceEntity::class);
    }

    public function store(Device $device): bool
    {
        return false;
    }

    public function getSubscribed(string $deviceId): array
    {
        return $this->findBy(['deviceId' => $deviceId, 'subscribed' => true]);
    }

    public function findForDeviceUser(DeviceUser $entity): array
    {
        return $this->findForClassAndIndentifier('Infostrates\AwsPush\Tests\Fixtures\Entity\UserEntity', $entity->getDeviceUserIdentifier());
    }

    public function findForClassAndIndentifier(string $deviceUserClass, string $deviceUserId): array
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.deviceUserIdentifier = :deviceUserIdentifier')
            ->setParameter('deviceUserIdentifier', $deviceUserId)
            ->getQuery()
            ->getResult();
    }
}
