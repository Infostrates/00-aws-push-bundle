<?php

/**
 * © Infostrates
 * Par julien
 * Le 29/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\Tests\Services;

use Doctrine\ORM\EntityManagerInterface;
use Infostrates\AwsPush\Tests\Fixtures\Entity\DeviceEntity;
use Infostrates\AwsPush\Tests\Fixtures\Entity\UserEntity;

final class FixtureLoader
{
    private EntityManagerInterface $emn;

    public function __construct(EntityManagerInterface $emn)
    {
        $this->emn = $emn;
    }

    public function execute(): void
    {
        $this->emn->getConnection()->executeStatement('DELETE FROM user_entity');
        $this->emn->getConnection()->executeStatement('DELETE FROM device_entity');

        $user = new UserEntity();
        $this->emn->persist($user);
        $this->emn->flush();
        $this->print('User created');

        $device = new DeviceEntity($user);
        $this->emn->persist($device);
        $this->emn->flush();
        $this->print('Device created');
    }

    private function print(string $print): void
    {
        echo sprintf("\e[42m%s\e[0m\n", $print);
    }
}
