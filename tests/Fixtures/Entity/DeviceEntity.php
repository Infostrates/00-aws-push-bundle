<?php

/**
 * © Infostrates
 * Par julien
 * Le 29/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\Tests\Fixtures\Entity;

use Doctrine\ORM\Mapping as ORM;
use Infostrates\AwsPush\Contract\DeviceInterface;

/**
 * @ORM\Entity()
 */
class DeviceEntity implements DeviceInterface
{
    public const TEST_DEVICE_ID = 'test-device-id';

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue()
     */
    private int $id;

    /**
     * @ORM\Column()
     */
    private string $deviceId = self::TEST_DEVICE_ID;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $subscribed = false;

    /**
     * @ORM\Column()
     */
    private string $deviceUserIdentifier;

    public function __construct(UserEntity $user)
    {
        $this->deviceUserIdentifier = $user->getDeviceUserIdentifier();
        $this->subscribe('en');
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function subscribe(string $locale): void
    {
        $this->subscribed = true;
    }

    public function unsubscribe(): void
    {
        $this->subscribed = false;
    }

    public function isSubscribed(): bool
    {
        return $this->subscribed;
    }

    public function getDeviceId(): string
    {
        return $this->deviceId;
    }

    public function getDeviceOs(): string
    {
        return 'ios';
    }

    public function getArn(): string
    {
        return 'test-device-arn';
    }

    public function getLocale(): string
    {
        return $this->locale = 'en';
    }

    public function getDeviceUserClass(): string
    {
        return UserEntity::class;
    }

    public function getDeviceUserIdentifier(): string
    {
        return $this->deviceUserIdentifier;
    }
}
