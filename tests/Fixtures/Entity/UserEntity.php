<?php

/**
 * © Infostrates
 * Par julien
 * Le 29/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\Tests\Fixtures\Entity;

use Doctrine\ORM\Mapping as ORM;
use Infostrates\AwsPush\Contract\DeviceUser;
use Infostrates\AwsPush\Domain\DeviceUserAwsMethods;

/**
 * @ORM\Entity()
 */
class UserEntity implements DeviceUser
{
    use DeviceUserAwsMethods;

    public const USER_ID = 'user-id';

    /**
     * @ORM\Id()
     * @ORM\Column()
     */
    public string $id = self::USER_ID;
}
