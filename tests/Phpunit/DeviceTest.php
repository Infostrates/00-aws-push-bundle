<?php

/**
 * © Infostrates
 * Par julien
 * Le 26/11/2021
 */

declare(strict_types=1);

namespace Infostrates\AwsPush\Tests\Phpunit;

use Doctrine\ORM\EntityManagerInterface;
use Infostrates\AwsPush\AWS\AwsResolver;
use Infostrates\AwsPush\AWS\AwsSnsConfig;
use Infostrates\AwsPush\AWS\Exception\DisabledConfigException;
use Infostrates\AwsPush\AWS\Exception\NotHandledPlatformException;
use Infostrates\AwsPush\Contract\DeviceInterface;
use Infostrates\AwsPush\Domain\DeviceGateway;
use Infostrates\AwsPush\Tests\Fixtures\Entity\DeviceEntity;
use Infostrates\AwsPush\Tests\Fixtures\Entity\UserEntity;
use Infostrates\AwsPush\Tests\Services\FixtureLoader;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

final class DeviceTest extends KernelTestCase
{
    private DeviceGateway $deviceGateway;

    private EntityManagerInterface $emn;

    private AwsResolver $awsResolver;

    public static function setUpBeforeClass(): void
    {
        self::bootKernel();
        $container = self::getContainer();
        (new FixtureLoader($container->get(EntityManagerInterface::class)))->execute();
    }

    protected function setUp(): void
    {
        $container = self::getContainer();
        $this->deviceGateway = $container->get(DeviceGateway::class);
        $this->awsResolver = $container->get(AwsResolver::class);
        $this->emn = $container->get(EntityManagerInterface::class);
    }

    /**
     * Test that no device has been created with this
     */
    public function testMissingDevice(): void
    {
        self::assertCount(0, $this->deviceGateway->getSubscribed('fake-id'));
    }

    /**
     * Test that one subscribed device (created by fixtures) is found with ID DeviceEntity::TEST_DEVICE_ID
     * Then that the same device is not found when unsubscribed
     */
    public function testFindDeviceById(): void
    {
        $subscribed = $this->deviceGateway->getSubscribed(DeviceEntity::TEST_DEVICE_ID);
        self::assertCount(1, $subscribed);
        /** @var DeviceInterface $device */
        $device = current($subscribed);
        $device->unsubscribe();
        $this->emn->flush();

        self::assertCount(0, $this->deviceGateway->getSubscribed(DeviceEntity::TEST_DEVICE_ID));
    }

    /**
     * Test that the device is found using the User::getDeviceUserIdentifier value
     * And that the DeviceUserEntityListener::postLoad hydrates the Collection
     *
     * Then test that the Device is found and not found using $onlySubscribed value
     */
    public function testFindDeviceByUser(): void
    {
        /** @var UserEntity $user */
        $user = $this->emn->getRepository(UserEntity::class)->find(UserEntity::USER_ID);

        self::assertCount(1, $this->deviceGateway->findForDeviceUser($user));
        self::assertCount(1, $user->getDevices()->toArray());
        self::assertNotNull($user->getDeviceById(DeviceEntity::TEST_DEVICE_ID));
        self::assertNull($user->getDeviceById(DeviceEntity::TEST_DEVICE_ID, true));
    }

    /**
     * Test that the test bundle config is well executed
     */
    public function testBundleConfig(): void
    {
        $config = $this->awsResolver->getCurrentConfig();
        self::assertEquals('test', $config->getConfig());
        self::assertEquals('test-topic-arn', $config->getAwsTopicArn());
        self::assertEquals('test-ios-application', $config->getAwsIosApplication());
        $this->expectException(NotHandledPlatformException::class);
        $this->expectExceptionMessage('Platform `Android` not handled by the application');
        $config->getAwsAndroidApplication();
    }

    /**
     * Test tant the disable_push config is well executed
     */
    public function testConfigIsDisabled(): void
    {
        $config = $this->awsResolver->getCurrentConfig();
        $this->expectException(DisabledConfigException::class);
        $this->expectExceptionMessage('The AwsSnsConfig `test` is disabled');
        $config->subscribe('failure');
    }
}
